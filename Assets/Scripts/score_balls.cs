﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class score_balls : MonoBehaviour {

    public TextMesh scoreText = null;
    private int Balls = 3;
    void Start()
    {
        scoreText = GameObject.Find("num_balls").GetComponent<TextMesh>();
    }

    void Update()
    {
        scoreText.text = "" + Balls;

    }

    public void AddBalls(int newScore)
    {
        Balls -= newScore;
    }

    public int getBalls()
    {
        return Balls;
    }

    public void setBalls()
    {
        Balls = 3;
    }


}
