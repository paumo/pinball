﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flippers : MonoBehaviour {

    public float force;
    Rigidbody rb;
    public Vector3 forceDirection = Vector3.forward;
    public AudioSource up;
    // Update is called once per frame
    void Start()
    {
        force = 2000;
        rb = this.gameObject.GetComponent<Rigidbody>();
    }

    void Update ()
    {
        if(Input.GetMouseButtonDown(0))
        {
            rb.AddForceAtPosition(forceDirection.normalized * force, transform.position);
            up.Play();
        }

        else if (Input.GetMouseButtonUp(0))
        {
            rb.AddForceAtPosition(forceDirection.normalized * -force, transform.position);
            
        }


    }
};
