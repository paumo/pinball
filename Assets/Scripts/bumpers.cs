﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bumpers : MonoBehaviour {

    public score newScore;
    private Rigidbody ballrb;
    private float force=400.0f;
    private float radius=1.0f;
    private Light lightBumper;
    public float valueScore;
    public AudioSource shock;
	void Start () {
        newScore = GameObject.Find("score_textNum").GetComponent<score>();
        ballrb = GameObject.Find("ball").GetComponent<Rigidbody>();
        lightBumper = this.GetComponent<Light>();
        valueScore = 1.0f;
        
	}


    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ball")
        {
            shock.Play();
            foreach (Collider hit in Physics.OverlapSphere(transform.position, radius))
            {
                if (hit.GetComponent<Rigidbody>()!=null)
                {
                    hit.GetComponent<Rigidbody>().AddExplosionForce(force * ballrb.velocity.magnitude, transform.position, radius);
                }
            }
            newScore.AddScore(valueScore);
            lightBumper.intensity = 10;
            Invoke("setLight", 0.30f);
        }
    }

    void setLight()
    {
        lightBumper.intensity = 0;
    }
	
    

}
