﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class targets : MonoBehaviour {

    public score newScore;
    public int numOfTargetsDown;
    float originz;
    float originx;
    private RigidbodyConstraints oFreeze;
    public AudioClip firstBlood;
    public AudioClip secondBlood;
    public AudioClip rampage;
    public AudioSource targetsMusic;
    void Start () {
        newScore = GameObject.Find("score_textNum").GetComponent<score>();
        targetsMusic = this.gameObject.GetComponent<AudioSource>();
        numOfTargetsDown = 1;
        originz = this.transform.localPosition.z;
        originx = this.transform.localPosition.x;


    }
	
	
	void Update () {
	}

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ball")
        {
            this.transform.localPosition = new Vector3(originx, -1f, originz);
            newScore.setTargetsDown(numOfTargetsDown);

            if(newScore.getTargetsDown()==1)
            {
                targetsMusic.PlayOneShot(firstBlood);
            }
            if (newScore.getTargetsDown() == 2)
            {
                targetsMusic.PlayOneShot(secondBlood);
            }
            if(newScore.getTargetsDown()==3)
            {
                targetsMusic.PlayOneShot(rampage);
            }


        }
        
    }
}
