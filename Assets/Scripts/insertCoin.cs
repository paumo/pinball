﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class insertCoin : MonoBehaviour
{
    public score newScore;
    public GameObject ball;
    public GameObject target1;
    public GameObject target2;
    public GameObject target3;
    public score_balls ballsScore;
    public AudioSource insert;
    public AudioSource music;
    public AudioSource musicBall;
    void Start()
    {
        ballsScore = GameObject.Find("num_balls").GetComponent<score_balls>();
        newScore = GameObject.Find("score_textNum").GetComponent<score>();
        musicBall = ball.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(2))
        {
            insert.Play();
            ball.transform.localPosition = new Vector3(7.65f, 0.4f, -0.96f);
            target1.transform.localPosition = new Vector3(-2.32f, 0.514f, 7.46f);
            target2.transform.localPosition = new Vector3(-0.16f, 0.514f, 7.46f);
            target3.transform.localPosition = new Vector3(2.12f, 0.514f, 7.46f);
            ballsScore.setBalls();
            newScore.setScore();
            musicBall.Stop();
            music.Play();

        }
        if (ballsScore.getBalls() == 0)
        {
            music.Stop();
        }
    }
}
