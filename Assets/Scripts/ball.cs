﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour {

    public int counter;
    public score_balls ballsScore;
    public int valueBalls;
    float originz;
    float originy;
    float originx;
    public AudioClip dead;
    public AudioClip GameOver;
    public AudioSource ballAudio;
    void Start () {
        counter = 3;
        valueBalls = 1;
        originz = this.transform.localPosition.z;
        originy = this.transform.localPosition.y;
        originx = this.transform.localPosition.x;
        ballsScore = GameObject.Find("num_balls").GetComponent<score_balls>();
        ballAudio = this.gameObject.GetComponent<AudioSource>();
    }
	
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "deadZone" && counter!=0)
        {
            this.transform.localPosition = new Vector3(originx, originy, originz);
            ballsScore.AddBalls(valueBalls);
            ballAudio.PlayOneShot(dead);
            counter--;

            if (counter==0)
            {
                this.transform.localPosition = new Vector3(originx, -1.37f, originz);
                ballAudio.PlayOneShot(GameOver);
                counter = 3;
            }
        }
    }

}
