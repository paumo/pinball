﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class score : MonoBehaviour {

    public TextMesh scoreText = null;
    private float Score = 0;
    private int NumTargetsDown = 0;
	void Start () {
        scoreText = GameObject.Find("score_textNum").GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
        scoreText.text = "" + Score;
    }

    public void AddScore(float newScore)
    {
        Score += newScore;
        
        if(getTargetsDown()>=3)
        {
            Score += newScore * 3;
        }
    }

    public void setScore()
    {
        Score = 0;
        NumTargetsDown = 0;
    }
    
   public void setTargetsDown(int target)
    {
        NumTargetsDown += target;
    }

    public int getTargetsDown()
    {
        return NumTargetsDown;
    }
}
